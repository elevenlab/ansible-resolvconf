Ansible Role: resolvconf
===================

Confiure nameserver for Debian like instance

----------
Requirment
--------------

None

Role variables
-------------

Available variables are listed below, along with default values (see `defaults/main.yml`):

	resolv_search_domain:
the search domain, default not set
	
	resolv_nameserver_ips: ['8.8.8.8', '8.8.4.4']
A list of nameserver ips, defaults to google public nameserver.

	resolv_backup_ns_ip: 8.8.8.8
The backup namserver if the previous fail, it defaults to 8.8.8.8


   
Dependencies
-------------------
None.

Example Playbook
--------------------------

    - name: allow passwordless access between hosts
      hosts: all
      vars:
        resolv_nameserver_ips:
          - '10.0.0.1'
          - '10.0.0.2'
        resolv_backup_ns_ip:
          - '8.8.8.8'
      roles:
        - resolvconf

